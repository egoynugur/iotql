(define 
  (problem iot) 
  (:domain iot) 
  (:objects device0 device1 device2 device3 device4 device5 device6 device7 device8 device9 device10 device11 device12 device13 device14 device15 device16 device17 device18 device19 mother someoneAtFrontDoor PlaySoundSpeaker SendSMS DisplayMessageOnTv) 
  (:init 
    (Person mother) 
    (Event someoneAtFrontDoor) 
    (SoundAction PlaySoundSpeaker) 
    (SMSAction SendSMS) 
    (VisualAction DisplayMessageOnTv) 
    (= 
    (total-cost) 0) 
    (canPerform device0 DisplayMessageOnTv) 
    (= 
      (p2Cost device0) 0) 
    (canPerform device1 PlaySoundSpeaker) 
    (= 
      (p1Cost device1) 0) 
    (canPerform device1 DisplayMessageOnTv) 
    (= 
      (p2Cost device1) 0) 
    (canPerform device1 SendSMS) 
    (= 
      (p3Cost device1) 0) 
    (canPerform device2 SendSMS) 
    (= 
      (p3Cost device2) 0) 
    (canPerform device3 SendSMS) 
    (= 
      (p3Cost device3) 0) 
    (canPerform device4 PlaySoundSpeaker) 
    (= 
      (p1Cost device4) 9) 
    (canPerform device4 DisplayMessageOnTv) 
    (= 
      (p2Cost device4) 0) 
    (canPerform device5 SendSMS) 
    (= 
      (p3Cost device5) 0) 
    (canPerform device6 DisplayMessageOnTv) 
    (= 
      (p2Cost device6) 10) 
    (canPerform device7 SendSMS) 
    (= 
      (p3Cost device7) 0) 
    (canPerform device8 DisplayMessageOnTv) 
    (= 
      (p2Cost device8) 0) 
    (canPerform device9 PlaySoundSpeaker) 
    (= 
      (p1Cost device9) 0) 
    (canPerform device9 SendSMS) 
    (= 
      (p3Cost device9) 0) 
    (canPerform device10 PlaySoundSpeaker) 
    (= 
      (p1Cost device10) 7) 
    (canPerform device11 PlaySoundSpeaker) 
    (= 
      (p1Cost device11) 0) 
    (canPerform device12 SendSMS) 
    (= 
      (p3Cost device12) 0) 
    (canPerform device13 PlaySoundSpeaker) 
    (= 
      (p1Cost device13) 0) 
    (canPerform device14 PlaySoundSpeaker) 
    (= 
      (p1Cost device14) 0) 
    (canPerform device14 SendSMS) 
    (= 
      (p3Cost device14) 0) 
    (canPerform device15 PlaySoundSpeaker) 
    (= 
      (p1Cost device15) 0) 
    (canPerform device16 PlaySoundSpeaker) 
    (= 
      (p1Cost device16) 0) 
    (canPerform device17 SendSMS) 
    (= 
      (p3Cost device17) 0) 
    (canPerform device18 SendSMS) 
    (= 
      (p3Cost device18) 0) 
    (canPerform device19 PlaySoundSpeaker) 
    (= 
      (p1Cost device19) 7) 
    (canPerform device19 DisplayMessageOnTv) 
    (= 
      (p2Cost device19) 0)) 
  (:goal 
    (and 
      (gotNotifiedFor mother someoneAtFrontDoor))) 
  (:metric minimize 
    (total-cost)))