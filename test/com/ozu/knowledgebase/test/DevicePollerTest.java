package com.ozu.knowledgebase.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.GsonBuilder;
import com.ozu.knowledgebase.devices.DevicePollerMaster;

public class DevicePollerTest {

	@Ignore("asd")
	@Test
	public void pollTest() throws InterruptedException, IOException {
        DevicePollerMaster devicePollerMaster = new DevicePollerMaster();
        devicePollerMaster.start();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter something to finish");
        String s = br.readLine();
        devicePollerMaster.stopThread();
	}
	
	@Test
	public void gson() throws InterruptedException, IOException {
		ArrayList<String> list = new ArrayList<String>();
		list.add("asd");
		list.add("sadf");
		list.add("czx");
		list.add("sadzx");
		
		HashSet<String> set = new HashSet<String>();
		set.add("asdxz");
		set.add("cxas");
		set.add("mxzc");
		set.add("129ednz");
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		System.out.println(gsonBuilder.create().toJson(list));
		System.out.println(gsonBuilder.create().toJson(set));
	}

}
