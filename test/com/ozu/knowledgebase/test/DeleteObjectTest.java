package com.ozu.knowledgebase.test;

import org.hibernate.Session;
import org.junit.Test;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.impl.Thing;

public class DeleteObjectTest {
	
	@Test
	public void deleteTest(){
		String name = "room1";
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.getTransaction().begin();
        Thing.deleteIndividualFromSession(name, session);
        session.getTransaction().commit();
        session.close();
	}

}
