package com.ozu.ql.database;

/**
 * Created by emre on 09/06/15.
 */

import java.util.HashMap;
import java.util.HashSet;

import com.ozu.ql.database.EPBQuery;

public class EPBQuery {
	private HashMap<String, HashSet<String>> selectVariables;
	private HashMap<String, HashSet<String>> unboundVariables;
	private HashSet<String> literals;
	private HashSet<String> tables;
	private boolean isAskQuery = false;

	private boolean where = false;

	public HashMap<String, HashSet<String>> getSelectVariables() {
		return selectVariables;
	}

	public HashMap<String, HashSet<String>> getUnboundVariables() {
		return unboundVariables;
	}

	public HashSet<String> getTables() {
		return tables;
	}
	
	public HashSet<String> getLiterals() {
		return literals;
	}

	public boolean getWhere() {
		return where;
	}

	public EPBQuery() {
		this.selectVariables = new HashMap<String, HashSet<String>>();
		this.unboundVariables = new HashMap<String, HashSet<String>>();
		this.tables = new HashSet<String>();
		this.literals = new HashSet<String>();
	}

	public EPBQuery(HashMap<String, HashSet<String>> selectVariables, HashMap<String, HashSet<String>> unboundVariables,
			HashSet<String> tables, HashSet<String> literals, boolean isAskQuery) {
		super(); 
		this.selectVariables = selectVariables;
		this.unboundVariables = unboundVariables;
		this.tables = tables;
		this.literals = literals;
		this.isAskQuery = isAskQuery;
	}

	public EPBQuery cloneQuery() {
		HashMap<String, HashSet<String>> _selectVariables = new HashMap<String, HashSet<String>>();
		for (String k : this.selectVariables.keySet())
			_selectVariables.put(k, new HashSet<String>(this.selectVariables.get(k)));

		HashMap<String, HashSet<String>> _unboundVariables = new HashMap<String, HashSet<String>>();
		;
		for (String k : this.unboundVariables.keySet())
			_unboundVariables.put(k, new HashSet<String>(this.unboundVariables.get(k)));

		HashSet<String> _tables = new HashSet<String>(this.tables);
		HashSet<String> _literals = new HashSet<String>(this.literals);


		return new EPBQuery(_selectVariables, _unboundVariables, _tables, _literals, isAskQuery);
	}

	public void mergeWith(EPBQuery q) {
		HashMap<String, HashSet<String>> _selectVariables = q.getSelectVariables();
		HashMap<String, HashSet<String>> _unboundVariables = q.getUnboundVariables();
		HashSet<String> _tables = q.getTables();
		HashSet<String> _literals = q.getLiterals();

		for (String key : _selectVariables.keySet()) {
			if (!this.selectVariables.containsKey(key))
				this.selectVariables.put(key, new HashSet<String>());

			this.selectVariables.get(key).addAll(_selectVariables.get(key));
		}

		for (String key : _unboundVariables.keySet()) {
			if (!this.unboundVariables.containsKey(key))
				this.unboundVariables.put(key, new HashSet<String>());

			this.unboundVariables.get(key).addAll(_unboundVariables.get(key));
		}
		
		this.tables.addAll(_tables);
		this.literals.addAll(_literals);
	}

	private HashMap<String, HashSet<String>> getAllVariables() {
		HashMap<String, HashSet<String>> all = new HashMap<String, HashSet<String>>();

		for (String key : selectVariables.keySet()) {
			if (!all.containsKey(key))
				all.put(key, new HashSet<String>());

			all.get(key).addAll(selectVariables.get(key));
		}

		for (String key : unboundVariables.keySet()) {
			if (!all.containsKey(key))
				all.put(key, new HashSet<String>());

			all.get(key).addAll(unboundVariables.get(key));
		}

		return all;
	}

	public boolean containsVar(String var) {
		return selectVariables.containsKey(var);
	}

	public String toSQLQuery() {
		String sql = "SELECT ";

		boolean first = true;

		if (!isAskQuery) {
			for (String var : selectVariables.keySet()) {
				String col = selectVariables.get(var).iterator().next();

				if (!first)
					sql += ", ";

				sql += col + " AS " + var;
				first = false;
			}
		} else
			sql += "1";
		
		first = true;
		sql += " FROM ";
		for (String table : tables) {

			if (!first)
				sql += ", ";

			sql += table;
			first = false;
		}

		first = true;

		HashMap<String, HashSet<String>> allVariables = getAllVariables();
		for (String var : allVariables.keySet()) {

			boolean left = true;
			boolean localFirst = true;

			if (allVariables.get(var).size() > 1) {
				String prev = "";
				for (String col : allVariables.get(var)) {

					if (!where) {
						sql += " WHERE ";
						where = true;
					}

					if (localFirst) {
						if (left) {
							if (!first)
								sql += " AND ";

							sql += col + "=";
							prev = col;
							left = false;
						} else {
							sql += col;
							left = true;
							localFirst = false;
						}
					} else {
						sql += " AND " + prev + "=" + col;
					}

					first = false;
				}
			}
		}
		
		for (String lit : literals){
			if (!where) {
				sql += " WHERE ";
				where = true;
				sql += lit;
			} else {
				sql += " AND " + lit;
			}
		}

		/*
		 * for (String table : tables){ if (!where) { sql += " WHERE "; where =
		 * true; }
		 * 
		 * if (!first) sql += " AND ";
		 * 
		 * sql += table + ".status=1"; first = false; }
		 */

		return sql;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((selectVariables == null) ? 0 : selectVariables.hashCode());
		result = prime * result + ((tables == null) ? 0 : tables.hashCode());
		result = prime * result + ((unboundVariables == null) ? 0 : unboundVariables.hashCode());
		result = prime * result + (where ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EPBQuery other = (EPBQuery) obj;
		if (selectVariables == null) {
			if (other.selectVariables != null)
				return false;
		} else if (!selectVariables.equals(other.selectVariables))
			return false;
		if (tables == null) {
			if (other.tables != null)
				return false;
		} else if (!tables.equals(other.tables))
			return false;
		if (unboundVariables == null) {
			if (other.unboundVariables != null)
				return false;
		} else if (!unboundVariables.equals(other.unboundVariables))
			return false;
		if (where != other.where)
			return false;
		return true;
	}

}
