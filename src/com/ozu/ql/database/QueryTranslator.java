package com.ozu.ql.database;

import java.util.*;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementFilter;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.hp.hpl.jena.sparql.syntax.ElementUnion;
import com.ozu.ql.database.EPBQuery;
import com.ozu.ql.database.EUQuery;

public class QueryTranslator {

	private int epbCounter;
	private boolean isAskQuery = false;

	public SQLQuery convertSPARQL2SQL(Query q) {
		/* TODO throw an exception? */
		if (q.getQueryType() != Query.QueryTypeSelect && q.getQueryType() != Query.QueryTypeAsk)
			return null;

		if (q.getQueryType() == Query.QueryTypeAsk)
			isAskQuery = true;

		epbCounter = 0;

		List<EPBQuery> epbQueries = new ArrayList<EPBQuery>();
		List<EUQuery> euQueries = new ArrayList<EUQuery>();

		HashMap<String, String> varColMap = new HashMap<String, String>();

		/*
		 * TODO Fix the expression later. Maybe convert it to a list of strings?
		 */
		// String expression = "";

		if (q.getQueryPattern() instanceof ElementGroup) {
			ElementGroup pattern = (ElementGroup) q.getQueryPattern();
			for (Element e : pattern.getElements()) {
				if (e instanceof ElementPathBlock) {
					epbQueries.add(translateElementPathBlock((ElementPathBlock) e));
				} else if (e instanceof ElementUnion) {
					euQueries.add(translateElementUnion((ElementUnion) e));
				} else if (e instanceof ElementGroup) {
					System.err.println("NOT IMPLEMENTED ELEMENTGROUP");
				} else if (e instanceof ElementFilter) {
					System.err.println("NOT IMPLEMENTED ELEMENTFILTER");
					/* TODO add to list */
					// expression = e.toString();
				} else {
					System.err.println("NOT IMPLEMENTED ELSE CONDITION");
				}
			}
		} else if (q.getQueryPattern() instanceof ElementUnion) {
			euQueries.add(translateElementUnion((ElementUnion) q.getQueryPattern()));
		} else if (q.getQueryPattern() instanceof ElementPathBlock) {
			epbQueries.add(translateElementPathBlock((ElementPathBlock) q.getQueryPattern()));
		}

		/*
		 * if (expression.length() > 0) { String[] split = expression.split(" "
		 * ); List<String> vars = new ArrayList<String>();
		 * 
		 * for (String s : split) if (s.startsWith("?")) vars.add(s);
		 * 
		 * Collections.sort(vars, new StringLengthComparator()); for (int i =
		 * vars.size() - 1; i >= 0; i--) { String key =
		 * vars.get(i).substring(1); if (varColMap.containsKey(key)) { String
		 * val = varColMap.get(key) + "." + key; expression =
		 * expression.replace(vars.get(i), val); } }
		 * 
		 * if (sql.contains("WHERE")) { expression =
		 * expression.replace("FILTER", ""); sql += " AND " + expression; } else
		 * { expression = expression.replace("FILTER", "WHERE"); sql += " " +
		 * expression; } }
		 */

		if (q.hasOrderBy())
			System.err.println("NOT IMPLEMENTED ORDER BY");

		if (q.hasGroupBy())
			System.err.println("NOT IMPLEMENTED GROUP BY");

		if (q.hasLimit())
			System.err.println("NOT IMPLEMENTED LIMIT");

		return new SQLQuery(epbQueries, euQueries, varColMap, isAskQuery);
	}

	private boolean isTypeAssertion(Triple tp) {
		return tp.getPredicate().getURI().contains("#type");
	}

	private boolean isTypeAssertion(TriplePath tp) {
		return tp.getPredicate().getURI().contains("#type");
	}

	private boolean isVariable(String var) {
		return !var.contains("unbound");
	}

	/* TODO implement */
	// private String convertOrderBy2SQL(List<SortCondition> orderBy) {
	// return " ORDER BY ";
	// }
	//
	// private String convertGroupBy2SQL(VarExprList groupBy) {
	// return " GROUP BY ";
	// }

	private EUQuery translateElementUnion(ElementUnion eu) {
		List<EUQuery> eus = new ArrayList<EUQuery>();
		List<EPBQuery> epbs = new ArrayList<EPBQuery>();
		String var = "";

		for (Element e : eu.getElements()) {
			if (e instanceof ElementPathBlock)
				epbs.add(translateElementPathBlock((ElementPathBlock) e));
			else if (e instanceof ElementUnion)
				eus.add(translateElementUnion((ElementUnion) e));
			else if (e instanceof ElementGroup)
				epbs.add(translateElementGroup((ElementGroup) e));
			else if (e instanceof ElementTriplesBlock)
				epbs.add(translateElementTriplesBlock((ElementTriplesBlock) e));
		}

		/*
		 * Select Variable should only have one element. Since this is a
		 * re-write of only ONE predicate
		 */
		if (!epbs.isEmpty())
			var = epbs.iterator().next().getSelectVariables().keySet().iterator().next();
		else if (!eus.isEmpty())
			var = eus.iterator().next().getVar();
		else
			System.err.println("Could not find the re-written variable");

		return new EUQuery(var, eus, epbs);
	}

	private EPBQuery translateElementTriplesBlock(ElementTriplesBlock e) {
		HashMap<String, HashSet<String>> selectVariables = new HashMap<String, HashSet<String>>();
		HashMap<String, HashSet<String>> unboundVariables = new HashMap<String, HashSet<String>>();
		HashSet<String> tables = new HashSet<String>();
		HashSet<String> literals = new HashSet<String>();

		for (Triple tp : e.getPattern().getList()) {
			if (isTypeAssertion(tp)) {
				String subject = tp.getSubject().getName();
				String object = tp.getObject().getLocalName();

				if (isVariable(subject))
					addVariable(subject, object + ".id", selectVariables);
				else
					addVariable(epbCounter + subject, object + ".id", unboundVariables);

				tables.add(object);
			} else {
				String subject = tp.getSubject().getName();
				String predicate = tp.getPredicate().getLocalName();

				if (!tp.getObject().isLiteral()) {
					String object = tp.getObject().getName();

					if (isVariable(subject))
						addVariable(subject, predicate + ".subjectid", selectVariables);
					else
						addVariable(epbCounter + subject, predicate + ".subjectid", unboundVariables);

					if (isVariable(object))
						addVariable(object, predicate + ".objectid", selectVariables);
					else
						addVariable(object, predicate + ".objectid", unboundVariables);

				} else {
					// data property
					String data = tp.getObject().getLiteralLexicalForm();
					addVariable(subject, predicate + ".subjectid", selectVariables);
					literals.add(predicate + ".data = " + "'" + data + "'");
				}

				tables.add(predicate);
			}
		}

		epbCounter++;

		return new EPBQuery(selectVariables, unboundVariables, tables, literals, isAskQuery);
	}

	// private String translateElementFilter(ElementFilter e) {
	// return null;
	// }

	private EPBQuery translateElementPathBlock(ElementPathBlock e) {
		HashMap<String, HashSet<String>> selectVariables = new HashMap<String, HashSet<String>>();
		HashMap<String, HashSet<String>> unboundVariables = new HashMap<String, HashSet<String>>();
		HashSet<String> tables = new HashSet<String>();
		HashSet<String> literals = new HashSet<String>();

		for (TriplePath tp : e.getPattern().getList()) {
			if (isTypeAssertion(tp)) {
				String subject = tp.getSubject().getName();
				String object = tp.getObject().getLocalName();

				if (isVariable(subject))
					addVariable(subject, object + ".id", selectVariables);
				else
					addVariable(epbCounter + subject, object + ".id", unboundVariables);

				tables.add(object);
			} else {
				String subject = tp.getSubject().getName();
				String predicate = tp.getPredicate().getLocalName();

				if (!tp.getObject().isLiteral()) {
					String object = tp.getObject().getName();

					if (isVariable(subject))
						addVariable(subject, predicate + ".subjectid", selectVariables);
					else
						addVariable(epbCounter + subject, predicate + ".subjectid", unboundVariables);

					if (isVariable(object))
						addVariable(object, predicate + ".objectid", selectVariables);
					else
						addVariable(object, predicate + ".objectid", unboundVariables);
				} else {
					// data property
					String data = tp.getObject().getLiteralLexicalForm();

					addVariable(subject, predicate + ".subjectid", selectVariables);
					literals.add(predicate + ".data = " + "'" + data + "'");

				}
				tables.add(predicate);
			}
		}

		epbCounter++;

		return new EPBQuery(selectVariables, unboundVariables, tables, literals, isAskQuery);
	}

	private void addVariable(String var, String val, HashMap<String, HashSet<String>> con) {

		if (!con.containsKey(var))
			con.put(var, new HashSet<String>());

		con.get(var).add(val);
	}

	private EPBQuery translateElementGroup(ElementGroup eg) {
		/* TODO For now it is assumed that ElementGroup only includes EPBs */
		List<EPBQuery> epbQueries = new ArrayList<EPBQuery>();

		for (Element e : eg.getElements()) {
			if (e instanceof ElementPathBlock) {
				epbQueries.add(translateElementPathBlock((ElementPathBlock) e));
			} else if (e instanceof ElementUnion) {
			} else if (e instanceof ElementGroup) {
			} else if (e instanceof ElementFilter) {

			} else {
			}
		}

		EPBQuery finalQuery = new EPBQuery();
		for (EPBQuery epbq : epbQueries)
			finalQuery.mergeWith(epbq);

		return finalQuery;
	}
}
