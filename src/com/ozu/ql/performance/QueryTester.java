package com.ozu.ql.performance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class QueryTester {
	public static void main(String[] args) throws IOException {
		String sql = "SELECT finalquery.p AS p, finalquery.t AS t, uniona.e AS e, unionb.sa AS sa FROM (SELECT NotifyAction.id AS sa FROM NotifyAction UNION SELECT SoundAction.id AS sa FROM SoundAction UNION SELECT PlaySound.id AS sa FROM PlaySound) AS unionb, (SELECT Event.id AS e FROM Event UNION SELECT gotNotifiedFor.objectid AS e FROM gotNotifiedFor UNION SELECT producedBy.subjectid AS e FROM producedBy UNION SELECT hasEvent.objectid AS e FROM hasEvent UNION SELECT produces.objectid AS e FROM produces) AS uniona,  (SELECT disabledNotificationType.subjectid AS p, disabledNotificationType.objectid AS t FROM SoundNotification, disabledNotificationType WHERE disabledNotificationType.objectid=SoundNotification.id) AS finalquery  WHERE finalquery.p IS NOT NULL AND finalquery.t IS NOT NULL AND uniona.e IS NOT NULL AND unionb.sa IS NOT NULL";
		QLQueryTester ql = new QLQueryTester();
		ql.executeSQLQuery(sql);
		/*QueryTester tester = new QueryTester();
		ArrayList<String> queries = tester.readQueries();
		
		PelletQueryTester pellet = new PelletQueryTester();
		pellet.setQueries(queries);
		pellet.executeQueries();
		
		QLQueryTester ql = new QLQueryTester();
		ql.init();
		ql.setQueries(queries);
		ql.executeQueries();*/
	}

	public ArrayList<String> readQueries() throws IOException {
		String json = new String(Files.readAllBytes(Paths.get("./benchmark/query.txt")));
		return new Gson().fromJson(json, new TypeToken<ArrayList<String>>() {
		}.getType());
	}
}
