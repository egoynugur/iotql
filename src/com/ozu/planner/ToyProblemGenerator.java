package com.ozu.planner;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;

public class ToyProblemGenerator {
	public static String[] capabilities = { "sound", "visual", "sms" };

	public static void main(String args[]) throws IOException {
		ArrayList<Integer> violations = new ArrayList<Integer>();
		int totalViolations = 0;
		
		for (int it = 0; it < 4; it++) {
			int deviceNo = 20;
			int prbCap = 3; // 0,3
			int prbPol = 2; // 0,2
			int prbBase = 10;
			int maxViolationCost = 10;
			int possibleViolations = 0;

			ArrayList<ToyDevice> devices = new ArrayList<ToyDevice>();

			for (int i = 0; i < deviceNo; i++) {
				System.out.println("Creating Device No: " + i);

				ToyDevice device = new ToyDevice("device" + Integer.toString(i));

				Random rand = new Random();
				for (int j = 0; j < capabilities.length; j++) {
					int rC = rand.nextInt(prbBase);
					System.out.println("CapProb: " + rC);
					if (rC < prbCap) {
						System.out.println("Adding capability: " + capabilities[j]);
						device.addCapability(capabilities[j]);

						int rP = rand.nextInt(prbBase);
						System.out.println("PolProb: " + rP);
						if (rP < prbPol) {
							System.out.println("Prohibiting action: " + capabilities[j]);
							int cost = rand.nextInt(maxViolationCost) + 1;
							device.addProhibition(capabilities[j], cost);
							possibleViolations++;
						}
					}
				}
				System.out.println("********************");
				System.out.println(device);
				System.out.println("********************");

				if (device.getCapabilities().size() == 0) {
					i--;
					continue;
				}

				devices.add(device);
			}

			// We have to choose the obliged action for the below methods to
			// create a conflict

			int iterations = 0;
			Random rand = new Random();
			int actor = -1;
			int obligationCost = rand.nextInt(maxViolationCost) + 1;
			int obligation = -1;
			boolean isProhibited = false;
			while (!isProhibited) {
				actor = rand.nextInt(devices.size());
				obligation = rand.nextInt(capabilities.length);
				isProhibited = (devices.get(actor).getProhibitions().containsKey(capabilities[obligation])) ? true
						: false;

				if (iterations == 50) {
					System.out.println("No conflict could be found");
					break;
				}

				iterations++;
			}
			
			if (!isProhibited){
				it--;
				continue;
			}

			int prohibitionCost = devices.get(actor).getProhibitions().get(capabilities[obligation]);

			System.out.println("Actor Device: " + actor);
			System.out.println(devices.get(actor));
			System.out.println("Obligation: (" + capabilities[obligation] + ", " + obligationCost + ")");

			String result = "Higher Cost: ";
			result += (obligationCost > prohibitionCost) ? (" Yes, " + prohibitionCost) : ("No, " + obligationCost);
			result += "\nAlways Prohibition: No, " + obligationCost;
			result += "\nAlways Obligation: Yes, " + prohibitionCost;

			String problem = generateProblemFile(devices, devices.get(actor));
			Path path = Paths.get("./problems/p-" + deviceNo + "-" + obligationCost + "-" + prohibitionCost + ".pddl");
			if (!path.toFile().exists())
				path.toFile().createNewFile();

			try (BufferedWriter writer = Files.newBufferedWriter(path)) {
				writer.write(problem);
			}

			path = Paths.get("./problems/p-" + deviceNo + "-" + obligationCost + "-" + prohibitionCost + ".txt");
			if (!path.toFile().exists())
				path.toFile().createNewFile();

			try (BufferedWriter writer = Files.newBufferedWriter(path)) {
				writer.write(result);
			}
			
			violations.add(possibleViolations);
			totalViolations += possibleViolations;
		}
		System.out.println ("");
		for (int i = 0; i < violations.size(); i++){
			System.out.print (violations.get(i));
			if (i != violations.size()-1)
				System.out.print(", ");
		}
		System.out.println ("");
		System.out.println(totalViolations/violations.size());
		System.out.println (totalViolations);
	}

	public static String generateProblemFile(ArrayList<ToyDevice> devices, ToyDevice actor) {
		LispExprList definition = new LispExprList();
		definition.add(new Atom("define"));

		LispExprList problem = new LispExprList();
		problem.add(new Atom("problem"));
		problem.add(new Atom("iot"));

		LispExprList domain = new LispExprList();
		domain.add(new Atom(":domain"));
		domain.add(new Atom("iot"));

		LispExprList objects = new LispExprList();
		objects.add(new Atom(":objects"));
		for (ToyDevice device : devices)
			objects.add(new Atom(device.getName()));
		objects.add(new Atom("mother"));
		objects.add(new Atom("someoneAtFrontDoor"));
		objects.add(new Atom("PlaySoundSpeaker"));
		objects.add(new Atom("SendSMS"));
		objects.add(new Atom("DisplayMessageOnTv"));

		LispExprList initialState = new LispExprList();
		initialState.add(new Atom(":init"));

		/* Boilerplate individuals */
		LispExprList mother = new LispExprList();
		mother.add(new Atom("Person"));
		mother.add(new Atom("mother"));

		LispExprList event = new LispExprList();
		event.add(new Atom("Event"));
		event.add(new Atom("someoneAtFrontDoor"));

		LispExprList soundAction = new LispExprList();
		soundAction.add(new Atom("SoundAction"));
		soundAction.add(new Atom("PlaySoundSpeaker"));

		LispExprList smsAction = new LispExprList();
		smsAction.add(new Atom("SMSAction"));
		smsAction.add(new Atom("SendSMS"));

		LispExprList visualAction = new LispExprList();
		visualAction.add(new Atom("VisualAction"));
		visualAction.add(new Atom("DisplayMessageOnTv"));

		LispExprList totalCost = new LispExprList();
		totalCost.add(new Atom("total-cost"));

		LispExprList initTotalCost = new LispExprList();
		initTotalCost.add(new Atom("="));
		initTotalCost.add(totalCost);
		initTotalCost.add(new Atom("0"));

		initialState.add(mother);
		initialState.add(event);
		initialState.add(soundAction);
		initialState.add(smsAction);
		initialState.add(visualAction);
		initialState.add(initTotalCost);
		/* Boilerplate ends here */

		for (ToyDevice device : devices) {
			for (String cap : device.getCapabilities()) {
				LispExprList canPerform = new LispExprList();
				canPerform.add(new Atom("canPerform"));
				canPerform.add(new Atom(device.getName()));

				String pId = "p";

				if (cap.equals("sound")) {
					canPerform.add(new Atom("PlaySoundSpeaker"));
					pId = "p1Cost";
				} else if (cap.equals("visual")) {
					canPerform.add(new Atom("DisplayMessageOnTv"));
					pId = "p2Cost";
				} else if (cap.equals("sms")) {
					canPerform.add(new Atom("SendSMS"));
					pId = "p3Cost";
				}

				initialState.add(canPerform);
				if (device.getProhibitions().containsKey(cap)) {
					initialState.add(getPDDLCostFunction(pId, device.getName(), device.getProhibitions().get(cap)));
				} else {
					initialState.add(getPDDLCostFunction(pId, device.getName(), 0));
				}
			}
		}

		LispExprList goalState = new LispExprList();
		goalState.add(new Atom(":goal"));

		LispExprList and = new LispExprList();
		and.add(new Atom("and"));

		LispExprList gotNotifiedFor = new LispExprList();
		gotNotifiedFor.add(new Atom("gotNotifiedFor"));
		gotNotifiedFor.add(new Atom("mother"));
		gotNotifiedFor.add(new Atom("someoneAtFrontDoor"));

		and.add(gotNotifiedFor);
		goalState.add(and);

		LispExprList metric = new LispExprList();
		metric.add(new Atom(":metric"));
		metric.add(new Atom("minimize"));
		metric.add(totalCost);

		definition.add(problem);
		definition.add(domain);
		definition.add(objects);
		definition.add(initialState);
		definition.add(goalState);
		definition.add(metric);

		return definition.toString();
	}

	public static LispExprList getPDDLCostFunction(String id, String name, int cost) {
		LispExprList costFunction = new LispExprList();
		costFunction.add(new Atom(id));
		costFunction.add(new Atom(name));

		LispExprList initCostFunction = new LispExprList();
		initCostFunction.add(new Atom("="));
		initCostFunction.add(costFunction);
		initCostFunction.add(new Atom(Integer.toString(cost)));

		return initCostFunction;
	}

}
