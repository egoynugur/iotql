package com.ozu.planner;

import static com.ozu.utils.StringConstants.ONTOLOGY_PATH_RDF;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;
import com.ozu.policy.PolicyGoal;

public class ProblemGenerator {

	public String generateProblemFile(PolicyGoal pGoal) throws OWLOntologyCreationException {
		LispExprList problem = new LispExprList();
		problem.add(new Atom("defproblem"));
		problem.add(new Atom("problem"));
		problem.add(new Atom("iot"));
		problem.add(getInitialState());
		problem.add(getGoal(pGoal));
		return problem.toString();
	}

	public LispExprList getGoal(PolicyGoal pGoal) {
		LispExprList goal = new LispExprList();
		LispExprList action = new LispExprList();
		action.add(new Atom(pGoal.getAction()));
		for (int i = 0; i < pGoal.getInputs().size(); i++)
			action.add(new Atom(pGoal.getInputs().get(i)));
		goal.add(action);
		return goal;
	}

	public LispExprList getInitialState() throws OWLOntologyCreationException {
		LispExprList initialState = new LispExprList();

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(ONTOLOGY_PATH_RDF));

		Set<OWLClass> classes = ontology.getClassesInSignature();
		for (OWLClass owlClass : classes) {
			/* TODO do not insert all individuals into OWLThing */
			if (owlClass.isOWLThing())
				continue;

			List<String> individuals = Thing.getIndividualNames(owlClass.getIRI().getShortForm());
			for (String ind : individuals) {
				LispExprList individual = new LispExprList();
				individual.add(new Atom(owlClass.getIRI().getShortForm()));
				individual.add(new Atom(ind.replaceAll("[^A-Za-z0-9 ]", "")));
				initialState.add(individual);
			}
		}


		Set<OWLObjectProperty> properties = ontology.getObjectPropertiesInSignature();
		for (OWLObjectProperty property : properties) {
			List<Object[]> objectProperties = ObjectProperty.getObjectPropertiesFrom(property.getIRI().getShortForm());
			for (Object[] entry : objectProperties) {
				LispExprList objProperty = new LispExprList();
				objProperty.add(new Atom(property.getIRI().getShortForm()));
				objProperty.add(new Atom(Thing.getIndividualName((Integer) entry[0]).replaceAll("[^A-Za-z0-9 ]", "")));
				objProperty.add(new Atom(Thing.getIndividualName((Integer) entry[1]).replaceAll("[^A-Za-z0-9 ]", "")));
				initialState.add(objProperty);
			}
		}

		return initialState;
	}
}
