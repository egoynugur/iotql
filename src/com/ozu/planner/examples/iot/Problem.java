package com.ozu.planner.examples.iot;

import java.util.LinkedList;
import JSHOP2.*;

import java.io.FileOutputStream;
import java.io.File;

import java.net.URL;
import java.net.URLClassLoader;
import static com.ozu.utils.StringConstants.PLANNER_INPUT_FOLDER;
public class Problem
{
	private static String[] defineConstants()
	{
		String[] problemConstants = new String[20];

		problemConstants[0] = "sound";
		problemConstants[1] = "room1";
		problemConstants[2] = "locationdiscoveryinflat";
		problemConstants[3] = "speaker";
		problemConstants[4] = "TemperatureSensorEmreRoomOutput";
		problemConstants[5] = "OutputSheet";
		problemConstants[6] = "tv";
		problemConstants[7] = "visual";
		problemConstants[8] = "TemperatureSensorEmreRoom";
		problemConstants[9] = "someoneAtFrontDoor";
		problemConstants[10] = "DoorbellOutput";
		problemConstants[11] = "flat1";
		problemConstants[12] = "location1";
		problemConstants[13] = "Location";
		problemConstants[14] = "PlaySoundSpeaker";
		problemConstants[15] = "mother";
		problemConstants[16] = "frontDoorBell";
		problemConstants[17] = "DisplayMessageOnTv";
		problemConstants[18] = "hasOutputSheet";
		problemConstants[19] = "locatedIn";

		return problemConstants;
	}

	private static void createState0(State s)	{
		s.add(new Predicate(22, 0, new TermList(TermConstant.getConstant(41), TermList.NIL)));
		s.add(new Predicate(38, 0, new TermList(TermConstant.getConstant(42), TermList.NIL)));
		s.add(new Predicate(3, 0, new TermList(TermConstant.getConstant(43), TermList.NIL)));
		s.add(new Predicate(31, 0, new TermList(TermConstant.getConstant(44), TermList.NIL)));
		s.add(new Predicate(30, 0, new TermList(TermConstant.getConstant(47), TermList.NIL)));
		s.add(new Predicate(40, 0, new TermList(TermConstant.getConstant(48), TermList.NIL)));
		s.add(new Predicate(36, 0, new TermList(TermConstant.getConstant(49), TermList.NIL)));
		s.add(new Predicate(0, 0, new TermList(TermConstant.getConstant(52), TermList.NIL)));
		s.add(new Predicate(14, 0, new TermList(TermConstant.getConstant(55), TermList.NIL)));
		s.add(new Predicate(8, 0, new TermList(TermConstant.getConstant(56), TermList.NIL)));
		s.add(new Predicate(25, 0, new TermList(TermConstant.getConstant(57), TermList.NIL)));
		s.add(new Predicate(19, 0, new TermList(TermConstant.getConstant(58), TermList.NIL)));
		s.add(new Predicate(16, 0, new TermList(TermConstant.getConstant(50), TermList.NIL)));
		s.add(new Predicate(39, 0, new TermList(TermConstant.getConstant(56), new TermList(TermConstant.getConstant(48), TermList.NIL))));
		s.add(new Predicate(39, 0, new TermList(TermConstant.getConstant(56), new TermList(TermConstant.getConstant(41), TermList.NIL))));
		s.add(new Predicate(11, 0, new TermList(TermConstant.getConstant(44), new TermList(TermConstant.getConstant(42), TermList.NIL))));
		s.add(new Predicate(11, 0, new TermList(TermConstant.getConstant(47), new TermList(TermConstant.getConstant(42), TermList.NIL))));
		s.add(new Predicate(2, 0, new TermList(TermConstant.getConstant(56), new TermList(TermConstant.getConstant(52), TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(TermConstant.getConstant(42), new TermList(TermConstant.getConstant(52), TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(TermConstant.getConstant(57), new TermList(TermConstant.getConstant(52), TermList.NIL))));
		s.add(new Predicate(18, 0, new TermList(TermConstant.getConstant(50), new TermList(TermConstant.getConstant(57), TermList.NIL))));
		s.add(new Predicate(6, 0, new TermList(TermConstant.getConstant(56), new TermList(TermConstant.getConstant(41), TermList.NIL))));
		s.add(new Predicate(34, 0, new TermList(TermConstant.getConstant(44), new TermList(TermConstant.getConstant(55), TermList.NIL))));
		s.add(new Predicate(34, 0, new TermList(TermConstant.getConstant(47), new TermList(TermConstant.getConstant(58), TermList.NIL))));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		TermConstant.initialize(61);

		Domain d = null;

		File source = new File(PLANNER_INPUT_FOLDER);
		try (URLClassLoader cl = new URLClassLoader(new URL[] { source.toURI().toURL() })) {
				Class<Iot> cls = (Class<Iot>) cl.loadClass("com.ozu.planner.examples.iot.Iot");
				d = cls.getConstructor().newInstance();
		}
		catch (Exception e){
		System.err.println("Error occured while creating domain object");
		}
		d.setProblemConstants(defineConstants());

		State s = new State(41, d.getAxioms());

		JSHOP2Imp.initialize(d, s);

		TaskList tl;
		SolverThread thread;

		createState0(s);

		tl = new TaskList(1, true);
		tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 0, new TermList(TermConstant.getConstant(52), new TermList(TermConstant.getConstant(50), TermList.NIL))), false, false));

		thread = new SolverThread(tl, 2147483647);
		thread.start();

		try {
			while (thread.isAlive())
				Thread.sleep(500);
		} catch (InterruptedException e) {
		}

		returnedPlans.addAll( thread.getPlans() );

		int counter = 0;
		for (Plan plan : returnedPlans){
			try {
				FileOutputStream fout = new FileOutputStream("plan-" + counter + ".sas"); 
				fout.write(plan.toString().getBytes());
				fout.flush();
				fout.close();
				counter++;
			} catch (Exception e) {;
				e.printStackTrace();
			}
			}
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}