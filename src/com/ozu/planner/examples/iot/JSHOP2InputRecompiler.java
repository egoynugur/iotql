package com.ozu.planner.examples.iot;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import static com.ozu.utils.StringConstants.PLANNER_INPUT_TARGET_FOLDER;
import static com.ozu.utils.StringConstants.PLANNER_INPUT_FOLDER;
import static com.ozu.utils.StringConstants.PLANNER_DOMAIN;
import static com.ozu.utils.StringConstants.PLANNER_PROBLEM;

import com.google.common.io.Files;

public class JSHOP2InputRecompiler {

	public static void compile() throws IOException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<JavaFileObject>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnosticsCollector, null, null);

		ArrayList<String> list = new ArrayList<String>();
		list.add(PLANNER_PROBLEM);
		list.add(PLANNER_DOMAIN);

		Iterable<JavaFileObject> fileObjects = (Iterable<JavaFileObject>) fileManager
				.getJavaFileObjectsFromStrings(list);
		CompilationTask task = compiler.getTask(null, fileManager, diagnosticsCollector, null, null, fileObjects);
		Boolean result = task.call();

		for (Object o : diagnosticsCollector.getDiagnostics()) {
			Diagnostic<JavaFileObject> diagnostic = (Diagnostic<JavaFileObject>) o;

			System.out.println("Code->" + diagnostic.getCode());
			System.out.println("Column Number->" + diagnostic.getColumnNumber());
			System.out.println("End Position->" + diagnostic.getEndPosition());
			System.out.println("Kind->" + diagnostic.getKind());
			System.out.println("Line Number->" + diagnostic.getLineNumber());
			System.out.println("Message->" + diagnostic.getMessage(Locale.ENGLISH));
			System.out.println("Position->" + diagnostic.getPosition());
			System.out.println("Source" + diagnostic.getSource());
			System.out.println("Start Position->" + diagnostic.getStartPosition());
			System.out.println("\n");
		}

		if (result == true) {
			System.out.println("Compilation has succeeded");

			String from = PLANNER_INPUT_FOLDER;
			String to = PLANNER_INPUT_TARGET_FOLDER;
			String[] files = new File(from).list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return (name.endsWith(".class"));
				}
			});

			for (String f : files) {
				File file = new File(from + f);
				Files.copy(file, new File(to + f));
				file.delete();
			}

		}

	}

}
