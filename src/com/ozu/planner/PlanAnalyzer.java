package com.ozu.planner;

import org.hibernate.Session;

import com.ozu.owlql.QLReasoner;
import com.ozu.owlql.ReasonerFactory;
import com.ozu.parser.LispExprList;
import com.ozu.parser.LispParser;
import com.ozu.parser.LispTokenizer;
import com.ozu.policy.InterveningPolicyReasoner;
import com.ozu.database.hibernate.HibernateUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by emre on 02/04/15.
 */
public class PlanAnalyzer {
	private final static int INCONSISTENT_COST = Integer.MAX_VALUE;

	private String planPath;
	private LispExprList plans;

	private double[] planCosts;

	private QLReasoner reasoner = null;
	private InterveningPolicyReasoner policyReasoner = null;
	private Session session = null;

	public PlanAnalyzer() {
	}

	public void readPlan(String planPathIn) {
		planPath = planPathIn;

		try {
			LispTokenizer tzr = new LispTokenizer(readFile(planPath, Charset.defaultCharset()));
			LispParser parser = new LispParser(tzr);
			LispParser.LispExpr lispExpr = parser.parseExpr();

			if (lispExpr instanceof LispExprList) {
				plans = (LispExprList) lispExpr;
				planCosts = new double[plans.size()];
			}

		} catch (IOException e) {
			System.err.println("Could not open the input file: " + e.toString());
		} catch (LispParser.ParseException e1) {
			System.err.println("Could not parse the domain file: " + e1.toString());
		}
	}

	public void preparePlanningSession() {

		if (this.session == null) {
			this.session = HibernateUtil.getSessionFactory().openSession();
			this.session.beginTransaction();
			this.policyReasoner = new InterveningPolicyReasoner(this.session);
			this.reasoner = new ReasonerFactory().getReasonerWithSession(this.session);
		} else {
			this.session.getTransaction().rollback();
			this.session.close();
			this.session = HibernateUtil.getSessionFactory().openSession();
			this.policyReasoner.setSession(this.session);
			this.reasoner.setSession(this.session);
		}
	}

	public int analyze() {
		try {

			readPlan("plan-details.out");

			if (plans == null || plans.size() <= 0)
				return -1;

			readCosts();

			for (int i = 0; i < plans.size(); i++) {
				LispExprList plan = (LispExprList) plans.get(i);

				if (plan.size() <= 1)
					continue;

				preparePlanningSession();

				policyReasoner.updateNormativeState();

				boolean isConsistent = false;
				System.out.println("Plan Title: " + plan.get(0));
				for (int j = 1; j < plan.size(); j++) {
					isConsistent = processOperator((LispExprList) plan.get(j), i);

					if (!isConsistent) {
						planCosts[i] = INCONSISTENT_COST;
						break;
					}
					policyReasoner.updateNormativeState();
				}
			}

			int bestPlan = chooseBestPlan();
			if (planCosts[bestPlan] == INCONSISTENT_COST) {
				System.err.println("Found plans are inconsistent!");
				return -1;
			} else {
				System.out.println("This is the best plan: " + bestPlan);
				return bestPlan;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	/* Returns the policy cost of action. If inconsistent returns -1 */
	private boolean processOperator(LispExprList operator, int planIndex) {

		if (operator.size() < 2)
			return true;

		LispExprList head = (LispExprList) operator.get(1);

		String msg = "Executing operator " + head.get(0);

		if (head.size() == 2)
			msg += " with parameter: ";
		else
			msg += " with parameters: ";

		for (int i = 1; i < head.size(); i++)
			msg += head.get(i) + " ";

		System.out.println(msg);

		boolean isConsistent = true;
		for (int i = 2; i < operator.size(); i++) {
			LispExprList action = (LispExprList) operator.get(i);

			switch (action.get(0).toString()) {
			case "add":
				isConsistent = addPredicates(action);
				break;
			case "delete":
				isConsistent = deletePredicates(action);
				break;
			default:
				isConsistent = true;
				System.err.println("Nothing happens: " + action.get(0).toString());

			}

			if (!isConsistent)
				return false;
			// TODO: Add newly activated obligation costs. Minimum of action and
			// violation costs
		}
		planCosts[planIndex] += policyReasoner.checkPolicyViolations(head);
		return isConsistent;
	}

	private void readCosts() {
		for (int i = 0; i < plans.size(); i++) {
			try (Stream<String> lines = Files.lines(new File("plan-" + String.valueOf(i) + ".sas").toPath())) {
				Optional<String> first = lines.findFirst();

				if (first.isPresent()) {
					String val = first.get().replace(" ", "");
					planCosts[i] += Double.valueOf(val.substring((val.indexOf(":") + 1)));
					System.out.println("Cost: " + String.valueOf(planCosts[i]));
				}
			} catch (IOException e) {
				System.err.println("IOException while reading costs from plan files: " + e.getMessage());
			}
		}
	}

	private int chooseBestPlan() {
		double min = Double.MAX_VALUE;
		int index = -1;

		for (int i = 0; i < planCosts.length; i++) {
			if (planCosts[i] < min) {
				min = planCosts[i];
				index = i;
			}
		}

		return index;
	}

	public boolean addPredicates(LispExprList statements) {
		return reasoner.addPredicates(statements);
	}

	public boolean deletePredicates(LispExprList statements) {
		return reasoner.deletePredicates(statements);
	}

	private String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
