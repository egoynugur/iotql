package com.ozu.utils;

/**
 * Created by emre on 20/05/15.
 */
public class StringConstants {
	public final static String IOT_QL_PATH = "/Users/emre/Documents/workspace/IoT/IoTQL";
	
	public final static String PLANNER_INPUT_TARGET_FOLDER = IOT_QL_PATH + "/target/classes/com/ozu/planner/examples/iot/";
	public final static String PLANNER_INPUT_FOLDER = IOT_QL_PATH + "/src/com/ozu/planner/examples/iot/";
	public final static String PLANNER_PROBLEM = IOT_QL_PATH + "/src/com/ozu/planner/examples/iot/Problem.java";
	public final static String PLANNER_DOMAIN = IOT_QL_PATH + "/src/com/ozu/planner/examples/iot/Iot.java";
	
    public final static String ONTOLOGY_IRI = "http://cs.ozyegin.edu.tr/muratsensoy/2015/03/sspn-ql#";
    public final static String ONTOLOGY_PATH_RDF = IOT_QL_PATH + "/sspn-ql-rdf.owl";
    
    public final static String POLICY_XML_PATH = IOT_QL_PATH + "/policies.xml";
    public final static String CLASS_QUERIES_TXT = IOT_QL_PATH + "/classQueries.txt";
    
    public final static String RDF_SYNTAX_TYPE = "rdf-syntax-ns#type";
    public final static String RDF_SYNTAX_ABOUT = "rdf-syntax-ns#about";
    
    public final static String HAS_BOOLEAN_VALUE = "hasBooleanValue";
    public final static String HAS_DOUBLE_VALUE = "hasDoubleValue";
    public final static String HAS_STRING_VALUE = "hasStringValue";
    
    public final static String XSD_BOOLEAN = "xsd:boolean";
    public final static String XSD_STRING = "xsd:string";
    public final static String XSD_DOUBLE = "xsd:double";
    /* TODO not supported in SenML */
//    public final static String XSD_LONG = "xsd:long";
    
    public final static String HYPERCAT_SENML = "application/senml+json";
}
