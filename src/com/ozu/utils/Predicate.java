package com.ozu.utils;

public class Predicate {
	
	private boolean isProperty;
	private String predicate;
	private String subject;
	private String object;
	
	public Predicate(boolean isProperty, String predicate, String subject, String object) {
		super();
		this.isProperty = isProperty;
		this.predicate = predicate;
		this.subject = subject;
		this.object = object;
	}

	public boolean isProperty() {
		return isProperty;
	}

	public String getPredicate() {
		return predicate;
	}

	public String getSubject() {
		return subject;
	}

	public String getObject() {
		return object;
	}

	@Override
	public String toString() {
		if (isProperty)
			return "(" + predicate + " " + subject + " " + object + ")";
		return "(" + object + " " + subject + ")";
	}
	
}
