package com.ozu.knowledgebase.hypercat.commands;

import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Query;
import org.hibernate.Session;

import com.ozu.hypercat.objects.Senml;
import com.ozu.hypercat.objects.SenmlParameter;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;

public class SenmlCommand {
	private Senml info;
	private Integer subject;

	private final static String HAS_OUTPUT = "hasOutput";
	private final static String HAS_UNIT = "hasUnit";

	public SenmlCommand(Senml info, Integer subject) {
		super();
		this.info = info;
		this.subject = subject;
	}

	private HashMap<String, SenmlParameter> getLatestEntries(ArrayList<SenmlParameter> parameters) {
		HashMap<String, SenmlParameter> latestEntries = new HashMap<String, SenmlParameter>();
		for (SenmlParameter curr : parameters) {
			if (curr.getName() == null || curr.getName().length() <= 0 || curr.getTime() == null)
				continue;

			String output = getShortName(curr.getName()) + "S" + subject;
			if (curr.getUnit() != null)
				output += "U" + getShortName(curr.getUnit());

			SenmlParameter old = latestEntries.get(output);
			if (old != null && old.getTime().longValue() < curr.getTime().longValue()) {
				latestEntries.replace(output, curr);
				continue;
			}
			latestEntries.put(output, curr);
		}
		return latestEntries;
	}

	private String getSelectQuery(String type, String unit) {
		if (unit == null || unit.length() <= 0)
			return "SELECT " + HAS_OUTPUT + ".objectid FROM " + HAS_OUTPUT + ", " + type + " WHERE " + HAS_OUTPUT
					+ ".objectid=" + type + ".id AND " + HAS_OUTPUT + ".subjectid=:subject";

		return "SELECT " + HAS_OUTPUT + ".objectid FROM " + HAS_OUTPUT + ", " + type + ", " + HAS_UNIT + " WHERE "
				+ HAS_OUTPUT + ".objectid=" + type + ".id AND " + HAS_OUTPUT + ".subjectid=:subject AND " + type
				+ ".id = " + HAS_UNIT + ".subjectid AND " + HAS_UNIT + ".objectid = :unit";
	}

	private void createEntry(String outputType, String outputName, Integer unit, String predicate, String data,
			EDataType dataType, Session session) {
		Integer output = Thing.addIndividualToSession(outputName, outputType, session);

		if (unit != null)
			ObjectProperty.addObjectPropertyToSession(HAS_UNIT, output, unit, session);

		DataProperty.addDataPropertyToSession(output, predicate, data, dataType, session);
		ObjectProperty.addObjectPropertyToSession(HAS_OUTPUT, subject, output, session);
	}

	private String getShortName(String uri) {
		return uri.substring(uri.indexOf("#") + 1);
	}

	public void execute(Session session) {
		/* TODO Inefficient? Use inferred individuals or directly tables? */
		/* Should we keep a copy old file and remove old entries? */

		/* Only get the most recent measurements. */
		HashMap<String, SenmlParameter> latestEntries = getLatestEntries(info.getParameters());
		for (String key : latestEntries.keySet()) {
			SenmlParameter sp = latestEntries.get(key);

			String outputType = getShortName(sp.getName());
			String select = getSelectQuery(outputType, sp.getUnit());

			Query q = session.createSQLQuery(select);
			q.setInteger("subject", subject);

			Integer unit = null;
			if (sp.getUnit() != null) {
				unit = Thing.getIndividualId(getShortName(sp.getUnit()), session);
				q.setInteger("unit", unit);
			}

			/* Each unit type should have one entry */
			Integer outputId = (Integer) q.uniqueResult();
			if (outputId == null)
				createEntry(outputType, key, unit, sp.getPredicate(), sp.getValueAsString(), sp.getValueType(),
						session);
			else // Update or Delete the old entry and add a new one?
				DataProperty.updateDataValueOneToOne(outputId, sp.getPredicate(), sp.getValueAsString(), session);
		}
	}

	@Override
	public String toString() {
		return "SenmlCommand [info=" + info + ", subject=" + subject + "]";
	}
}
