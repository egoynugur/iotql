package com.ozu.knowledgebase.hypercat.commands;

import org.hibernate.Session;

import com.ozu.hypercat.objects.HypercatObject;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;

public class DeleteInformation extends HypercatCommand{
	
	public DeleteInformation (HypercatObject info){
		this.info = info;
	}

	@Override
	public void execute(Session session) {
		this.session = session;
		Integer subject = DataProperty.getSubjectId(info.getHref(), "hasHref", session);
		Thing.deleteIndividualFromSession(subject, session);
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString() {
		return "Delete Information: " + info.toString();
	}

}
