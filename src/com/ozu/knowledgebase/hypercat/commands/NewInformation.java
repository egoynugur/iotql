package com.ozu.knowledgebase.hypercat.commands;

import static com.ozu.utils.StringConstants.RDF_SYNTAX_ABOUT;

import java.util.HashMap;

import org.hibernate.Session;

import com.ozu.hypercat.objects.HypercatCatalogue;
import com.ozu.hypercat.objects.HypercatObject;
import com.ozu.hypercat.objects.HypercatTriple;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;

public class NewInformation extends HypercatCommand {

	public NewInformation(HypercatObject info) {
		this.info = info;
		this.cache = new HashMap<String, Integer>();
		this.session = null;
	}

	@Override
	public void execute(Session session) {
		this.session = session;

		newInformation(info);

		if (info instanceof HypercatCatalogue) {
			for (HypercatObject item : ((HypercatCatalogue) info).getItems())
				newInformation(item);
		}

		this.session = null;
	}

	private void newInformation(HypercatObject item) {

		/*
		 * TODO can this be more efficient so that we only iterate metadata
		 * once?
		 */

		/*
		 * We need individual name to create this item in the ontology. If we
		 * cannot find a name then we don't create that object. Create an item
		 * with a generated name?
		 */
		String name = "";
		for (HypercatTriple metadata : item.getMetadata()) {
			if (metadata.getRel().contains(RDF_SYNTAX_ABOUT)) {
				name = extractName(metadata);
				break;
			}
		}

		/* TODO raise exception or give error? */
		if (name.length() < 1) {
			System.err.println("Could not find individual name triple: " + RDF_SYNTAX_ABOUT);
			return;
		}

		try {
			Integer current = getOrCreateIndividual(name);
			for (HypercatTriple metadata : item.getMetadata())
				addTriple(current, metadata);

			if (item.getHref() != null && item.getHref().length() > 0)
				DataProperty.addDataPropertyToSession(current, "hasHref", item.getHref(), EDataType.STRING, session);
		} catch (Exception e) {
			System.err.println("Exception Processing New Item: " + info);
			session.getTransaction().rollback();
			e.printStackTrace();
			return;
		}
		return;
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
	}

	@Override
	public String toString() {
		return "New Information: " + info.toString();
	}

}
