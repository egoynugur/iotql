package com.ozu.knowledgebase.devices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.Stream;

import com.ozu.knowledgebase.devices.DeviceAction.ActionStatus;
import com.ozu.planner.PlanAnalyzer;
import com.ozu.planner.examples.iot.PlannerJSHOP2;
import com.ozu.policy.ActivePolicy;
import com.ozu.policy.PolicyGoal;

public class DeviceObligationObserver extends Thread {
	private PriorityBlockingQueue<ActivePolicy> obligations;
	private boolean quit = false;

	public DeviceObligationObserver(PriorityBlockingQueue<ActivePolicy> obligations) throws IOException {
		this.obligations = obligations;
	}

	public void stopThread() {
		quit = true;
	}

	public boolean executePlan(int planNo) {
		System.out.println("Executing plan no: " + planNo);

		try {
			List<DeviceAction> actions = getActionsFromPlanOutput("plan-" + planNo + ".sas");
			for (DeviceAction a : actions) {
				if (a.execute() == ActionStatus.FAILED) {
					cleanPlanOutputs();
					return false;
				}
				/* TODO try another plan? */
			}

			/* Remove plan results */
			cleanPlanOutputs();
		} catch (IOException e) {
			System.err.println("Could not open the input file: " + e.toString());
		}
		return true;
	}

	@Override
	public void run() {
		while (!quit) {
			ActivePolicy ob = null;
			try {
				ob = obligations.take();
				System.out.println("Consume: " + ob);
				for (PolicyGoal goal : ob.getGoal()) {
					new PlannerJSHOP2().startPlanner(goal);
					int result = new PlanAnalyzer().analyze();
					if (result != -1)
						if (!executePlan(result))
							break;
					/*
					 * TODO failed - revcover from the situation
					 */
					// TODO executed policies should not be re-activated
				}
			} catch (Exception e) {
				System.err.println("DeviceObligationObserver: " + e.getMessage());
				e.printStackTrace();
				if (ob != null)
					obligations.add(ob);
			}
		}
	}

	private void cleanPlanOutputs() throws IOException {
		/* TODO Find a unique name for each planning problem? */
//		Object[] array = Files.list(new File(".").toPath()).filter(p -> (p.getFileName().toString().contains(".sas")
//				|| p.getFileName().toString().contains("plan-details.out"))).toArray();
//		for (Object p : array)
//			Files.deleteIfExists((Path) p);
	}

	private List<DeviceAction> getActionsFromPlanOutput(String plan) throws IOException {
		Object[] array;
		try (Stream<String> lines = Files.lines(Paths.get(plan))) {
			array = lines.filter(s -> s.contains("(!")).toArray();
		}

		List<DeviceAction> list = new ArrayList<DeviceAction>();
		for (Object o : array)
			list.add(DeviceAction.createInstanceFromPlanOutput((String) o));

		return list;
	}

}
