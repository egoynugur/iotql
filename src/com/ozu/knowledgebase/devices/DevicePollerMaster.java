package com.ozu.knowledgebase.devices;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hibernate.Session;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;

public class DevicePollerMaster extends Thread {

	private final static int THREAD_NUMBER = 5;
	private final static int DEFAULT_POLL_INTERVAL = 1;
	// private final static String outputSheetRel = "HasOutputSheet";
	private final static String outputSheetCls = "OutputSheet";

	private ScheduledExecutorService scheduledThreadPool;
	private boolean quit;

	public DevicePollerMaster() {
		super();
		scheduledThreadPool = Executors.newScheduledThreadPool(THREAD_NUMBER);
		quit = false;
	}

	public void stopThread() {
		quit = true;
	}

	@Override
	public void run() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			List<Integer> individuals = Thing.getIndividualIds(outputSheetCls);
			for (Integer outputSheet : individuals) {
				double pollInterval = Double
						.parseDouble(DataProperty.getDataValue(outputSheet, "hasPollInterval", session));
				String outputLink = DataProperty.getDataValue(outputSheet, "hasHref", session);

				if (pollInterval <= 0)
					pollInterval = DEFAULT_POLL_INTERVAL;

				scheduledThreadPool.scheduleWithFixedDelay(new Thread(new DevicePollerSlave(outputSheet, outputLink)),
						0, (long) pollInterval, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}

		try {
			while (true) {
				Thread.sleep(40000);

				if (quit)
					break;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		scheduledThreadPool.shutdown();
		while (!scheduledThreadPool.isTerminated()) {
			// wait for all tasks to finish
		}
		System.out.println("Finished all threads");
	}
}
