package com.ozu.knowledgebase.devices;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.GsonBuilder;
import com.ozu.hypercat.objects.Senml;
import com.ozu.knowledgebase.KnowledgeBaseManager;
import com.ozu.knowledgebase.hypercat.commands.SenmlCommand;

public class DevicePollerSlave implements Runnable {

	private String url;
	private Integer outputSheet;
	private long lastModified;

	// private PriorityQueue<String>

	@Override
	public void run() {
		pollSensor();
	}

	public DevicePollerSlave(Integer outputSheet, String url) {
		super();
		this.lastModified = -1;
		this.url = url;
		this.outputSheet = outputSheet;
	}

	public void pollSensor() {
		HttpRequest request = HttpRequest.get(url);
		if (request.ok()) {
			if (lastModified != request.lastModified() && request.lastModified() != -1) {
				lastModified = request.lastModified();
				Senml info = (new GsonBuilder().create()).fromJson(request.body(), Senml.class);

				try {
					KnowledgeBaseManager.getInstance().updateFromSenmlFile(new SenmlCommand(info, outputSheet));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("Output file has been changed");
				System.out.println(info);
			}
			System.out.println("No change in the output file");
		}
	}

}
