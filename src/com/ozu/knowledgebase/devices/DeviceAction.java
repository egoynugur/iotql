package com.ozu.knowledgebase.devices;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.github.kevinsawicki.http.HttpRequest;
import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;

public class DeviceAction {

	private String service;
	private String instance;
	private List<String> parameters;
	private ActionStatus status;

	public enum ActionStatus {
		NOT_EXECUTED, FAILED, SUCCESSFUL
	}

	public DeviceAction(String service, String instance, List<String> parameters) {
		super();
		this.service = service;
		this.instance = instance;
		this.parameters = parameters;
		this.status = ActionStatus.NOT_EXECUTED;
	}

	public ActionStatus execute() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Integer id = Thing.getIndividualId(instance, session);
			String url = DataProperty.getDataValue(id, "hashref", session);

			HttpRequest request = HttpRequest.get(url, true, "parameter", parameters);
			if (request.code() == 200)
				status = ActionStatus.SUCCESSFUL;
			else
				status = ActionStatus.FAILED;
			
			/* TODO do something with the response body? */
//			request.body();

		} catch (Exception e) {
			/* TODO change exception type to hibernate exception */
			session.getTransaction().rollback();
			status = ActionStatus.FAILED;
		} finally {
			session.close();
		}
		return status;
	}

	public static DeviceAction createInstanceFromPlanOutput(String output) {
		String[] parsedOutput = output.substring(2, output.length() - 1).split(" ");

		if (parsedOutput.length > 1) {
			String service = parsedOutput[0];
			String instance = parsedOutput[1];

			List<String> parameters = new ArrayList<String>();
			for (int i = 2; i < parsedOutput.length; i++)
				parameters.add(parsedOutput[i]);

			return new DeviceAction(service, instance, parameters);
		}

		return null;
	}

	@Override
	public String toString() {
		return "DeviceAction [service=" + service + ", instance=" + instance + ", parameters=" + parameters
				+ ", status=" + status + "]";
	}

}
