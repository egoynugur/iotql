package com.ozu.hypercat.objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;
//import com.ozu.utils.Pair;

@Entity
public class HypercatTriple {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Expose
	private String rel;
	@Expose
	private String val;
	
	public HypercatTriple () {}
	
	public HypercatTriple (String r, String v){
		rel = r;
		val = v;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rel == null) ? 0 : rel.hashCode());
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HypercatTriple other = (HypercatTriple) obj;
		if (rel == null) {
			if (other.rel != null)
				return false;
		} else if (!rel.equals(other.rel))
			return false;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		return true;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	boolean isValid (){
		if (rel == null || val == null)
			return false;
		
		if (rel.isEmpty() || val.isEmpty())
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return "HypercatTriple [rel=" + rel + ", val=" + val + "]";
	}
}
