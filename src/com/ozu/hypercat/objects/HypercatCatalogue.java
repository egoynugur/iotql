package com.ozu.hypercat.objects;

import static com.ozu.hypercat.objects.Rel.CATALOGUE_JSON;
import static com.ozu.hypercat.objects.Rel.CONTENT_TYPE;
import static com.ozu.hypercat.objects.Rel.HAS_DESCRIPTION;
import static com.ozu.hypercat.objects.Rel.SIMPLE_SEARCH;
import static com.ozu.hypercat.objects.Rel.SUPPORTS_SEARCH;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import com.ozu.knowledgebase.hypercat.HypercatInfo;
//import com.ozu.knowledgebase.hypercat.HypercatInfoComposite;
//import com.ozu.utils.Pair;

@Entity
public class HypercatCatalogue extends HypercatObject {	
	
	private String pathString;
	
	@Expose
	@SerializedName("item-metadata")
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable
	private Set<HypercatTriple> metadata = new HashSet<HypercatTriple>();
	
	@Expose
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable
	protected List<HypercatObject> items = new ArrayList<HypercatObject>();

	public HypercatCatalogue () {
		addMetadata (CONTENT_TYPE, CATALOGUE_JSON);
	}
	
	public HypercatCatalogue (String description) {
		this();
		addDescription(description);
	}

	public void enableSearch () {
		addMetadata (SUPPORTS_SEARCH,SIMPLE_SEARCH);
	}
	
	public HypercatItem convertToItem(){
		HypercatItem item = new HypercatItem ();
		item.setHref(href);
		item.setMetadata(metadata);
		return item;
	}
	
	public boolean isValid(){
		boolean validContentType = false, validDescription = false;
		for (HypercatTriple data : metadata){
			if (!data.isValid())
				return false;
			
			if (data.getRel().equals(CONTENT_TYPE) && data.getVal().equals(CATALOGUE_JSON))
				validContentType = true;
			if (data.getRel().equals(HAS_DESCRIPTION))
				validDescription = true;
		}
		
		if (validContentType && validDescription){
			for (HypercatObject item : items){
				if (!item.isValid())
					return false;
			}
		}else {
			return false;
		}
		
		return true;
	}
	
	@Override
	public Set<HypercatTriple> getMetadata() {
		return metadata;
	}
	
	public void setMetadata(HashSet<HypercatTriple> metadata) {
		this.metadata = metadata;
	}
	
	public List<HypercatObject> getItems() {
		return items;
	}

	public void setItems(List<HypercatObject> items) {
		this.items = items;
	}
	
	public void setPathString(String pathString) {
		this.pathString = pathString;
	}
	
	public String getPathString() {
		return pathString;
	}
	
	public void addItem (HypercatObject item){
		addItem(item, false);
	}
	
	public void addItem (HypercatObject item, boolean override){
		if(override == true){
			removeItem(item.getHref());
		}
		items.add(item);
	}

	public void removeItem(String href) {
		HypercatObject found = null;
		for (HypercatObject hypercatObject : items) {
			if(hypercatObject.getHref().equals(href)){
				found = hypercatObject;
				break;
			}
		}
		
		if(found != null)
			items.remove(found);
	}

	@Override
	public String toString() {
		return "HypercatCatalogue [pathString=" + pathString + ", metadata="
				+ metadata + ", items=" + items + "]";
	}

	public HypercatObject getItem(String href) {
		for (HypercatObject hypercatObject : items) {
			if(hypercatObject.getHref().equals(href)){
				return hypercatObject;
			}
		}
		return null;
	}
}