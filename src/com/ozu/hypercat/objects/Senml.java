package com.ozu.hypercat.objects;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

/* Sensor Markup Language */
public class Senml {
	@Expose
	// Base Name
	private String bn;
	@Expose
	// Base Units
	private String bu;
	@Expose
	// Base Time
	private Number bt;
	@Expose
	// Base Version
	private Number ver;
	@Expose
	// Parameters
	private ArrayList<SenmlParameter> e;
	
	public String getBaseName() {
		return bn;
	}
	public Number getBaseTime() {
		return bt;
	}
	public String getBaseUnits() {
		return bu;
	}
	public Number getVersion() {
		return ver;
	}
	public ArrayList<SenmlParameter> getParameters() {
		return e;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bn == null) ? 0 : bn.hashCode());
		result = prime * result + ((bt == null) ? 0 : bt.toString().hashCode());
		result = prime * result + ((bu == null) ? 0 : bu.hashCode());
		result = prime * result + ((e == null) ? 0 : e.hashCode());
		result = prime * result + ((ver == null) ? 0 : ver.toString().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Senml other = (Senml) obj;
		if (bn == null) {
			if (other.bn != null)
				return false;
		} else if (!bn.equals(other.bn))
			return false;
		if (bt == null) {
			if (other.bt != null)
				return false;
		} else if (bt.longValue() != other.bt.longValue())
			return false;
		if (bu == null) {
			if (other.bu != null)
				return false;
		} else if (!bu.equals(other.bu))
			return false;
		if (e == null) {
			if (other.e != null)
				return false;
		} else if (!e.equals(other.e))
			return false;
		if (ver == null) {
			if (other.ver != null)
				return false;
		} else if (ver.longValue() != other.ver.longValue())
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Senml [bn=" + bn + ", bt=" + bt + ", bu=" + bu + ", ver=" + ver
				+ ", e=" + e + "]";
	}
	
	
}
