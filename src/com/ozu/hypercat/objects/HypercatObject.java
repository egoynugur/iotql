package com.ozu.hypercat.objects;

import static com.ozu.hypercat.objects.Rel.HAS_DESCRIPTION;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.google.gson.annotations.Expose;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class HypercatObject {
	@Id
	@GeneratedValue
	protected int id;
	
	@Expose
	protected String href= null;
	
	public abstract Set<HypercatTriple> getMetadata();
	public abstract boolean isValid();

	public void addDescription (String description){
		addMetadata (HAS_DESCRIPTION, description==null?"":description);
	}

	public void addMetadata (String rel, String val){
		this.getMetadata().add(new HypercatTriple (rel, val));
	}

	//TODO search and find rel...
	public void removeMetadata (HypercatTriple triple){
		this.getMetadata().remove(triple);
	}
	
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
}