package com.ozu.hypercat.json.deserializers;

import com.google.gson.GsonBuilder;
import com.ozu.hypercat.objects.HypercatItem;

public class JsonHypercatConverter {
	public HypercatItem jsonToHypercat (String json, String href){
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(HypercatItem.class, new ItemDeserializer());
	    
	    HypercatItem item = (gsonBuilder.create()).fromJson(json, HypercatItem.class);
	    item.setHref(href);
	    
	    if (item.isValid())
	    	return item;
	    else
	    	return null;
	}
	
}
