package com.ozu.hypercat.json.deserializers;

import java.lang.reflect.Type;
import java.util.HashSet;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.HypercatTriple;

public class ItemDeserializer implements JsonDeserializer<HypercatItem>{

	@Override
	public HypercatItem deserialize(JsonElement json, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {

	    final JsonObject jsonObject = json.getAsJsonObject();
	    final HypercatItem item = new HypercatItem();
	    final Gson gson = new Gson();
	    
	    Type metaList = new TypeToken<HashSet<HypercatTriple>>() {}.getType();
	    item.setMetadata(gson.fromJson(jsonObject.get("i-object-metadata").getAsJsonArray(), metaList));
	    item.setHref(jsonObject.get("href").getAsString());
	    
	    return item;
	}
}
