package com.ozu.owlql;

import java.util.*;

import com.hp.hpl.jena.graph.Triple;

import com.ibm.research.owlql.ConjunctiveQuery;
import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;
import com.ozu.parser.LispParser;
import com.ozu.planner.PredicateSub;

public class QL2JSHOP2 {

    private QLQueryRewriter qr;
    private static final Set<String> CONNECTORS = new HashSet<String>(Arrays.asList("not", "and", "or", "forall"));

    public QL2JSHOP2() {
        qr = QLQueryRewriter.getInstance();
    }

    public LispExprList rewritePredicate(LispExprList e) {
    	if (e.size() == 0)
    		return new LispExprList();
        if (!CONNECTORS.contains(e.get(0).toString()) && e.get(0) instanceof Atom) {
            if (e.size() == 2)
                return getExtendedTypePredicate(e);
            else if (e.size() == 3)
                return getExtendedPropertyPredicate(e);
            return e;
        } else {
            LispExprList newExp = new LispExprList();
            int start = 0;

            if (e.get(0) instanceof Atom){
            	newExp.add(new Atom(e.get(0).toString()));
            	start++;
            }

            if (e.get(0).toString().equals("forall"))
                return forallPredicate(e, newExp);

            for (int i = start; i < e.size(); i++)
                newExp.add(rewritePredicate((LispExprList) e.get(i)));

            return newExp;
        }
    }

    private LispExprList forallPredicate(LispExprList e, LispExprList newExp) {
        LispExprList inputs = new LispExprList();

		/* This statement must be true forall to be parsed */
        LispExprList params = (LispExprList) e.get(1);
        for (LispParser.LispExpr i : params)
            inputs.add(new Atom(i.toString()));

        newExp.add(inputs);
        newExp.add(rewritePredicate((LispExprList) e.get(2)));
        newExp.add((LispExprList) ((LispExprList) e.get(3)));

        return newExp;
    }

    private PredicateSub convertQueryToSHOP2(ConjunctiveQuery q) {
        PredicateSub mainQuery = new PredicateSub();

        Set<ConjunctiveQuery> nqs = qr.rewriteQuery(q);
        for (ConjunctiveQuery qq : nqs) {

            PredicateSub subQuery = new PredicateSub();

            List<Triple> triples = qq.getTriples();
            for (Triple triple : triples) {
                PredicateSub ps = new PredicateSub();

                ps.setPredicate(triple.getPredicate().toString());
                ps.setSubject(triple.getSubject().toString());
                ps.setObject(triple.getObject().toString());

                subQuery.addAdjacent(ps);
            }
            mainQuery.addAlternative(subQuery);
            System.out.println(qq + "\n----------------------");
        }
        return mainQuery;
    }

    private LispExprList getExtendedTypePredicate(LispExprList e) {
        ConjunctiveQuery q = qr.getMembershipQuery(e.get(0).toString(),
                e.get(1).toString().replace("?", ""));
        return convertQueryToSHOP2(q).toSHOP2();
    }

    private LispExprList getExtendedPropertyPredicate(LispExprList e) {
        ConjunctiveQuery q = qr.getPropertyQuery(e.get(0).toString(),
                e.get(1).toString().replace("?", ""),
                e.get(2).toString().replace("?", ""));
        return convertQueryToSHOP2(q).toSHOP2();
    }

}
