package com.ozu.owlql;

import static com.ozu.utils.StringConstants.ONTOLOGY_IRI;
import static com.ozu.utils.StringConstants.ONTOLOGY_PATH_RDF;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.rdf.rdfxml.parser.RDFConstants;
import org.semarglproject.vocab.XSD;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock;
import com.ibm.research.owlql.ConjunctiveQuery;
import com.ibm.research.owlql.OWLQLCompiler;
import com.ibm.research.owlql.ruleref.OWLQLToNonRecursiveDatalogCompiler;
import com.ozu.utils.SchemaDateFormat;

import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;

/**
 * Created by emre on 07/04/15.
 */
public class QLQueryRewriter {
	private OWLOntologyManager manager;
	private OWLDataFactory fac;
	private OWLOntology qlOnt;
	private OWLQLCompiler compiler;
	private OWLQLToNonRecursiveDatalogCompiler datalogCompiler;
	private static QLQueryRewriter instance = null;

	protected QLQueryRewriter() {
	}

	public OWLQLCompiler getCompiler() {
		return compiler;
	}

	public void setCompiler(OWLQLCompiler compiler) {
		this.compiler = compiler;
	}
	
	public static QLQueryRewriter getInstance() {
		if (instance == null) {
			// Thread Safe. Might be costly operation in some case
			synchronized (QLQueryRewriter.class) {
				if (instance == null) {
					instance = new QLQueryRewriter();

					try {
						long startTime = System.currentTimeMillis();
						instance.init();
						long elapsedTime = System.currentTimeMillis() - startTime;
						System.out.println("Elapsed Time: " + elapsedTime);
					} catch (OWLOntologyCreationException e) {
						System.err.println("Could not create ontology - QLQueryRewriter: " + e.getMessage());
						instance = null;
					}
				}
			}
		}
		return instance;
	}

	private void init() throws OWLOntologyCreationException {
		fac = new OWLDataFactoryImpl();
		manager = OWLManager.createOWLOntologyManager();
		qlOnt = manager.loadOntologyFromOntologyDocument(new File(ONTOLOGY_PATH_RDF));// .createOntology();
		//compiler = new OWLQLCompiler(qlOnt);
		datalogCompiler = new OWLQLToNonRecursiveDatalogCompiler(qlOnt, null, null);
	}

	public Query rewriteCondition(String predicate) {
		return rewriteConditions(new String[] { predicate }, null);
	}
	
	public Query rewriteConditions(String[] predicates, Set<String> systemFunctions) {

		ElementTriplesBlock pattern = new ElementTriplesBlock();

		/* TODO remove ontology short name. just predicates should be enough? */
		Set<String> variables = new HashSet<String>();
		for (String p : predicates) {
			if (p.contains("sspn:")) {
				String pred = p.substring(p.indexOf("'") + 1, p.lastIndexOf("'"));
				String[] params = p.substring(p.indexOf("(") + 1, p.indexOf(")")).split(",");

				if (params.length == 1) {
					String var = params[0].replace("?", "");
					pattern.addTriple(getMembershipTriple(pred, var));
					variables.add(var);
				} else if (params.length == 2) {
					String sub = params[0].replace("?", "");
					// object property
					if (params[1].charAt(0) == '?') {
						String obj = params[1].replace("?", "");
						pattern.addTriple(getObjectPropertyTriple(sub, pred, obj));
						variables.add(sub);
						variables.add(obj);
					} else {
						// data property
						pattern.addTriple(getDataPropertyTriple(sub, pred, params[1]));
						variables.add(sub);
					}
				}
			} else if (p.contains("sys:")) {
				systemFunctions.add(p.substring(p.indexOf("sys:") + 4).replace("'", ""));
			}
		}

		ConjunctiveQuery cq = new ConjunctiveQuery();
		cq.setQueryPattern(pattern);

		for (String v : variables)
			cq.addResultVar(v);

		cq.setDistinct(true);

		return datalogCompiler.compileToUnionQuery(cq);
	}

	public Triple getObjectPropertyTriple(String s, String rel, String o) {
		OWLObjectProperty owlproperty = fac.getOWLObjectProperty(IRI.create(ONTOLOGY_IRI + rel));
		Node sub = createQueryNode(s);
		Node pred = NodeFactory.createURI(owlproperty.getIRI().toString());
		Node obj = createQueryNode(o);
		return new Triple(sub, pred, obj);
	}

	public Triple getMembershipTriple(String cls, String var) {
		OWLClass owlclass = fac.getOWLClass(IRI.create(ONTOLOGY_IRI + cls));
		Node sub = createQueryNode(var);
		Node pred = NodeFactory.createURI(RDFConstants.RDF_TYPE);
		Node obj = NodeFactory.createURI(owlclass.getIRI().toString());
		return new Triple(sub, pred, obj);
	}

	public Triple getDataPropertyTriple(String s, String rel, String d) {
		OWLDataProperty owlproperty = fac.getOWLDataProperty(IRI.create(ONTOLOGY_IRI + rel));
		Node sub = createQueryNode(s);
		Node pred = NodeFactory.createURI(owlproperty.getIRI().toString());
		Node data = getDataNode(d);
		return new Triple(sub, pred, data);
	}

	public Node createQueryNode(String var) {
		try {
			URL url = new URL(var);
			/*
			 * OWLIndividual ind = fac .getOWLNamedIndividual(IRI .create(var));
			 */
			return NodeFactory.createURI(IRI.create(url).toString());
		} catch (Exception e) {
			return NodeFactory.createVariable(var);
		}
	}
	

	public ConjunctiveQuery getPropertyQuery(String rel, String s, String o) {
		Triple qt = getObjectPropertyTriple(s, rel, o);
		ElementTriplesBlock p = new ElementTriplesBlock();
		p.getPattern().add(qt);
		ConjunctiveQuery cq = new ConjunctiveQuery();
		cq.setQueryPattern(p);
		cq.addResultVar(s);
		cq.addResultVar(o);
		cq.setDistinct(true);
		return cq;
	}

	public ConjunctiveQuery getMembershipQuery(String cls, String var) {
		ElementTriplesBlock p = new ElementTriplesBlock();
		Triple qt = getMembershipTriple(cls, var);
		p.getPattern().add(qt);
		ConjunctiveQuery cq = new ConjunctiveQuery();
		cq.setQueryPattern(p);
		cq.addResultVar(var);
		cq.setDistinct(true);
		return cq;
	}

	public Set<ConjunctiveQuery> rewriteQuery(ConjunctiveQuery q) {
		return datalogCompiler.compile(q);
	}

	private Node getDataNode(String data) {
		if (data.charAt(0) == '\"')
			return NodeFactory.createLiteral(data.replace("\"", ""), XSD.STRING);

		if (SchemaDateFormat.isDateTime(data))
			return NodeFactory.createLiteral(data, XSD.TIME);

		try {
			Integer.parseInt(data);
			return NodeFactory.createLiteral(data, XSD.INT);
		} catch (NumberFormatException e) {
			System.out.println(data + " is not a int");
		}

		try {
			Double.parseDouble(data);
			return NodeFactory.createLiteral(data, XSD.DOUBLE);
		} catch (NumberFormatException e) {
			System.out.println(data + " is not a double");
		}

		try {
			Boolean.parseBoolean(data);
			return NodeFactory.createLiteral(data, XSD.BOOLEAN);
		} catch (NumberFormatException e) {
			System.out.println(data + " is not a boolean");
		}

		return null;
	}

}
