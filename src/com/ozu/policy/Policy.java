package com.ozu.policy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.hp.hpl.jena.query.Query;
import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ql.database.QueryTranslator;
import com.ozu.ql.database.SQLQuery;

/**
 * Created by emre on 07/04/15.
 */
public class Policy {
	private String addressee;
	private String modality;
	private String name;

	private double cost;

	private List<PolicyGoal> goal;

	/* UntouchedMembers are used for query freezing */
	private String oAddresseeRole;
	private List<String> oActivation;
	private List<String> oExpiration;

	private String actionVar;
	private List<String> oActionDescription;

	private SQLQuery addresseeRole;
	private SQLQuery activation;
	private SQLQuery expiration;
	private SQLQuery deadline;
	private SQLQuery actionDescription;

	public String getName() {
		return name;
	}

	public String getModality() {
		return modality;
	}

	public String getAddressee() {
		return addressee;
	}

	public SQLQuery getExpiration() {
		return expiration;
	}

	public void setExpiration(SQLQuery expiration) {
		this.expiration = expiration;
	}
	
	public SQLQuery getActivationQuery(){
		return activation;
	}

	public String getOriginalAddresseeRole() {
		return oAddresseeRole;
	}

	public void setOriginalAddresseeRole(String oAddresseeRole) {
		this.oAddresseeRole = oAddresseeRole;
	}

	public List<String> getOriginalActionDescription() {
		return oActionDescription;
	}

	public void setOriginalActionDescription(List<String> oActionDescription) {
		this.oActionDescription = oActionDescription;
	}
	
	public void setOriginalExpiration (List<String> oExpiration) {
		this.oExpiration = oExpiration;
	}

	public List<String> getOriginalActivation() {
		return oActivation;
	}

	public void setOriginalActivation(List<String> oActivation) {
		this.oActivation = oActivation;
	}

	public String getActionVar() {
		return actionVar;
	}

	public void setActionVar(String actionVar) {
		this.actionVar = actionVar;
	}

	public SQLQuery getActionDescription() {
		return actionDescription;
	}

	public void setActionDescription(Query query) {
		this.actionDescription = convertConditions(query, null);
	}

	public Policy(String addressee, Query addresseeRole, String modality, String name, double cost,
			Query activation, Set<String> activationSysFunctions, Query expiration, Set<String> expirationSysFunctions,
			Query deadlineQueries, Set<String> deadlineSysFunctions, List<PolicyGoal> goal) {

		this.addressee = addressee;
		this.modality = modality;
		this.name = name;
		this.cost = cost;

		this.addresseeRole = convertConditions(addresseeRole, null);
		this.activation = convertConditions(activation, activationSysFunctions);
		this.expiration = convertConditions(expiration, expirationSysFunctions);
		this.deadline = convertConditions(deadlineQueries, deadlineSysFunctions);
		this.goal = goal;
	}

	public List<PolicyGoal> getGoal() {
		return goal;
	}

	public SQLQuery convertConditions(Query query, Set<String> sysFunctions) {
		if (query != null)
			return new QueryTranslator().convertSPARQL2SQL(query);
		// return new
		// QueryTranslator().convertSPARQL2SQL(ConjunctiveQuery.mergeQueries(queries,
		// sysFunctions));
		return null;
	}

	public HashSet<ActivePolicy> executeActivationConditions() {
		return executeActivationConditions(null);
	}

	/* TODO move this part to the policy reasoner? */
	public HashSet<ActivePolicy> executeActivationConditions(Session session) {

		HashSet<ActivePolicy> activeInstances = new HashSet<ActivePolicy>();
		/* TODO */
		boolean created = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().openSession();
				created = true;
			}

			List<Map<String, Integer>> results = session.createSQLQuery(activation.toString())
					.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();

			/*
			 * HashMap<String, String> nameCache = new HashMap<String,
			 * String>(); HashMap<String, String> idCache = new HashMap<String,
			 * String>();
			 */
			/* TODO Cache Values From Tables */
			for (int i = 0; i < results.size(); i++) {
				HashMap<String, String> bindingIds = new HashMap<String, String>();
				HashMap<String, String> bindingNames = new HashMap<String, String>();

				convertBindingsFromResultSet(results.get(i), bindingNames, bindingIds, session);

				activeInstances.add(createNewActivePolicy(bindingIds, bindingNames));
			}
		} catch (Exception e) {
			System.err.println("SQL Activation Condition Error: " + e.getMessage());
		} finally {
			if (created && session != null)
				session.close();
		}

		for (ActivePolicy a : activeInstances)
			System.out.println(a);

		return activeInstances;
	}

	private void convertBindingsFromResultSet(Map<String, Integer> result, HashMap<String, String> bindingNames,
			HashMap<String, String> bindingIds, Session session) {
		for (String key : result.keySet()) {
			bindingIds.put(key, result.get(key).toString());

			String name = (String) session
					.createSQLQuery("SELECT name FROM Thing where id =" + result.get(key).toString() + "")
					.uniqueResult();
			bindingNames.put(key, name);
		}
	}

	public ActivePolicy createNewActivePolicy(HashMap<String, String> bindingIds,
			HashMap<String, String> bindingNames) {
		List<PolicyGoal> _goal = PolicyGoal.bindGoals(bindingNames, goal);

		/* TODO */
		SQLQuery _activation = this.activation.cloneQuery();
		_activation.setBindings(bindingIds);

		SQLQuery _expiration = null;
		List<String> goalState = null;
		if (this.expiration != null) {
			_expiration = this.expiration.cloneQuery();
			_expiration.setBindings(bindingIds);
			
			goalState = new ArrayList<String>();
			
			for (String p : this.oExpiration){
				String pred = p.substring(p.indexOf("'") + 1, p.lastIndexOf("'"));
				String[] params = p.substring(p.indexOf("(") + 1, p.indexOf(")")).split(",");
				
				String newPred = pred + "(";
				for (int i = 0; i < params.length; i++){
					if (i == 1)
						newPred += "," + bindingNames.get(params[i].substring(1));
					else
						newPred += bindingNames.get(params[i].substring(1));
				}
				newPred += ")";
				
				goalState.add(newPred);
			}
			
		}

		SQLQuery _deadline = null;
		if (this.deadline != null) {
			_deadline = this.deadline.cloneQuery();
			_deadline.setBindings(bindingIds);
		}
		
		SQLQuery _action = null;
		if(this.actionDescription != null){
			_action = this.actionDescription.cloneQuery();
			_action.setBindings(bindingIds);
		}


		ActivePolicy active =  new ActivePolicy(addressee, modality, name, cost, _activation, _expiration, _deadline, _goal,
				bindingNames);
		active.setGoalState(goalState);
		active.setAction(_action);
		return active;
	}

	@Override
	public String toString() {
		BasicFormatterImpl sqlFormatter = new BasicFormatterImpl();

		String ret = "\nPolicy [Name: " + name + ", Modality: " + modality + ", Addressee: " + addressee + "]";
		ret += "\n\n\tAddresseeRole: " + addresseeRole;

		ret += "\n\n\tActivation Conditions: ";
		ret += "\t" + sqlFormatter.format(activation.toString());

		ret += "\n\n\tGoals: " + goal.toString();

		ret += "\n\n\tExpiration Conditions: ";
		if (expiration != null)
			ret += "\t" + sqlFormatter.format(expiration.toString());

		ret += "\n\n\tDeadline Conditions: ";
		if (deadline != null)
			ret += sqlFormatter.format(deadline.toString());

		ret += "\n\tCost: " + String.valueOf(cost);
		ret += "\n-------------------------------------------------------";

		return ret;
	}

}
