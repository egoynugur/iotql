package com.ozu.policy;

import java.util.*;

/**
 * Created by emre on 18/04/15.
 */
public class PolicyGoal {
	private String action;
	private List<String> inputs;

	public PolicyGoal(String action, List<String> inputs) {
		this.action = action;
		this.inputs = new ArrayList<String>(inputs);
	}

	public static PolicyGoal policyGoalfromString(String g) {
		String action = g.substring(g.indexOf("'") + 1, g.lastIndexOf("'"));
		String[] inputs = g.substring(g.indexOf("(") + 1, g.indexOf(")")).split(",");

		return new PolicyGoal(action, Arrays.asList(inputs));
	}

	public static List<PolicyGoal> policyGoalfromStringSet(Set<String> set) {
		List<PolicyGoal> returnSet = new ArrayList<PolicyGoal>();

		for (String s : set)
			returnSet.add(policyGoalfromString(s));

		return returnSet;
	}

	public PolicyGoal clonePolicyGoal() {
		return new PolicyGoal(action, new ArrayList<String>(inputs));
	}

	/* Test the hashcode */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PolicyGoal that = (PolicyGoal) o;

		if (Objects.equals(action, that.action)) {
			if (inputs.size() != that.inputs.size())
				return false;

			for (String in : inputs) {
				if (!that.inputs.contains(in))
					return false;
			}

			return true;
		}

		return Objects.equals(action, that.action) && Objects.equals(inputs, that.inputs);
	}

	@Override
	public int hashCode() {
		String in = "";
		ArrayList<String> copy = new ArrayList<>(inputs);
		Collections.sort(copy);
		for (String i : copy)
			in += i;
		return Objects.hash(action, in);
	}

	public static List<PolicyGoal> bindGoals(HashMap<String, String> bindings, List<PolicyGoal> goals) {
		List<PolicyGoal> returnSet = new ArrayList<PolicyGoal>();

		for (PolicyGoal goal : goals) {
			String a = goal.getAction();
			List<String> in = new ArrayList<String>();

			for (String s : goal.getInputs())
				in.add(bindings.get(s.replace("?", "")));

			returnSet.add(new PolicyGoal(a, in));
		}

		return returnSet;
	}

	public String getAction() {
		return action;
	}

	public List<String> getInputs() {
		return inputs;
	}

	@Override
	public String toString() {
		return "PolicyGoal [action=" + action + ", inputs=" + inputs + "]";
	}

}
