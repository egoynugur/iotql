package com.ozu.main;

import com.ozu.planner.PlanAnalyzer;
import com.ozu.policy.Policy;
import com.ozu.policy.PolicyManager;
import com.ozu.policy.PolicyReasoner;

public class QLSHOP2 {

	public static <E> void main(String[] args) throws Exception {
//		PlannerJSHOP2.startPlanner();

		PolicyManager.getInstance().readPolicies();

		PolicyReasoner policyReasoner = new PolicyReasoner();
		policyReasoner.updateNormativeState();
		
		for (Policy p : policyReasoner.getAllPolicies()){
			System.out.println(p.getActivationQuery().toString());
		}

		PlanAnalyzer analyzer = new PlanAnalyzer();
		analyzer.readPlan("plan-details.out");
		analyzer.analyze();
	}
}
